# Scripts

Here, I memo scripts that I have used during development.

## Postgres

### Run Postgres Container

```
docker run --name ndb -p 5432:5432 -e POSTGRES_PASSWORD=pass -d postgres

```

If you already create the same container before then you can just start the container

```
docker start ndb

```

This cmdlet will create Postgres instance so that you can connect to a database with:
* database: postgres (default)
* username: postgres (default)
* password: pass
* post: 5432 (default)

### Getting into the Postgres container

```
docker exec -i -t ndb bash
```

Then you will see the containers bash as a root user.

### Connect to a database

```
psql -d postgres -U postgres
```

### Query Databases

```
\l
```

### Query Tables

```
\dt
```

### Quit

```
\q
```

## application.properties

### Datasource

```
spring.datasource.username=postgres
spring.datasource.password=pass
spring.datasource.url=jdbc:postgresql://localhost:5432/postgres
spring.datasource.driver-class-name=org.postgresql.Driver
```

### Hibernate

```
spring.jpa.hibernate.ddl-auto=create-drop
spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation=true
spring.jpa.properties.hibernate.format_sql=true

logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE
```

### Test Database

```
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.url=jdbc:h2:mem:testdb
spring.datasource.driver-class-name=org.h2.Driver
```
