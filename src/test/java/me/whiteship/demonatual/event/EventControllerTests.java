package me.whiteship.demonatual.event;

import me.whiteship.demonatual.common.BaseControllerTests;
import me.whiteship.demonatual.common.Description;
import me.whiteship.demonatual.user.UserService;
import org.assertj.core.internal.bytebuddy.utility.RandomString;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders;

import java.time.LocalDateTime;
import java.util.stream.IntStream;

import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.hypermedia.HypermediaDocumentation.*;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class EventControllerTests extends BaseControllerTests {

    @Autowired
    EventRepository eventRepository;

    @Autowired
    UserService userService;

    private String bearer() {
        try {
            return "Bearer " + getAccessToken();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Description("Create event with correct data")
    @Test
    public void createEvent201() throws Exception {
        // Given
        EventDto.CreateOrUpdate eventDto = createEventDto();

        // When & Then
        mockMvc.perform(post("/api/events")
                    .header(HttpHeaders.AUTHORIZATION, bearer())
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(objectMapper.writeValueAsString(eventDto)))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().exists("Location"))
                .andExpect(jsonPath("id").isNumber())
                .andExpect(jsonPath("offline").value(true))
                .andExpect(jsonPath("free").value(false))
                .andExpect(jsonPath("owner.id").hasJsonPath())
                .andExpect(jsonPath("owner.password").doesNotExist())
                .andExpect(jsonPath("owner.email").doesNotExist())
                .andExpect(jsonPath("_links.self").hasJsonPath())
                .andExpect(jsonPath("_links.update").hasJsonPath())
                .andExpect(jsonPath("_links.events").hasJsonPath())
                .andDo(document("create-event",
                    links(
                        linkWithRel("self").description("link to self"),
                        linkWithRel("profile").description("link to profile"),
                        linkWithRel("update").description("link to update new event"),
                        linkWithRel("events").description("link to events")
                    ),
                    requestFields(
                        fieldWithPath("name").description("name of new event").type(String.class),
                        fieldWithPath("description").description("description of new event").type(String.class),
                        fieldWithPath("beginEnrollmentDateTime").description("date time of begin of new event")
                            .type(LocalDateTime.class),
                        fieldWithPath("closeEnrollmentDateTime").description("date time of close of new event")
                            .type(LocalDateTime.class),
                        fieldWithPath("beginEventDateTime").description("date time of begin of new event")
                            .type(LocalDateTime.class),
                        fieldWithPath("endEventDateTime").description("date time of end of new event")
                            .type(LocalDateTime.class),
                        fieldWithPath("basePrice").description("base price of new event")
                            .type(Integer.class).optional(),
                        fieldWithPath("maxPrice").description("max price of new event")
                            .type(Integer.class).optional(),
                        fieldWithPath("location").description("location of new event")
                                .type(String.class).optional(),
                        fieldWithPath("limitOfEnrollment").description("limit of enrollment of new event")
                            .type(Integer.class)
                    ),
                    responseHeaders(
                        headerWithName(HttpHeaders.LOCATION).description("URI to new event")
                    ),
                    relaxedResponseFields(
                        fieldWithPath("id").description("identifier of new event")
                    )
                ))
        ;
    }

    private EventDto.CreateOrUpdate createEventDto() {
        return EventDto.CreateOrUpdate.builder()
                    .name("test " + RandomString.make())
                    .description("test event")
                    .beginEnrollmentDateTime(LocalDateTime.of(2018, 10, 30, 8, 0))
                    .closeEnrollmentDateTime(LocalDateTime.of(2018, 11, 5, 23, 59))
                    .beginEventDateTime(LocalDateTime.of(2018, 11, 10, 9, 0))
                    .endEventDateTime(LocalDateTime.of(2018, 11, 10, 14, 0))
                    .location("어디선가..")
                    .basePrice(0)
                    .maxPrice(20000)
                    .build();
    }

    @Description("Fail to create event because of wrong data. Testing data binding with DTO and @Valid")
    @Test
    public void createEventFailWithDataBinding400() throws Exception {
        // Given
        EventDto.CreateOrUpdate eventDto = EventDto.CreateOrUpdate.builder().build();

        // When & Then
        mockMvc.perform(post("/api/events")
                    .header(HttpHeaders.AUTHORIZATION, bearer())
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(objectMapper.writeValueAsString(eventDto)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.content[0].objectName").hasJsonPath())
                .andExpect(jsonPath("$.content[0].field").hasJsonPath())
                .andExpect(jsonPath("$.content[0].rejectedValue").hasJsonPath())
                .andExpect(jsonPath("$.content[0].defaultMessage").hasJsonPath())
                .andExpect(jsonPath("$.content[0].code").hasJsonPath())
                .andDo(document("error",
                    relaxedResponseFields(
                        fieldWithPath("content").description("Error content")
                    ),
                    links(
                        linkWithRel("index").description("Link to index")
                    )
                ));
    }

    @Description("Fail to create event because of wrong data, Testing validation with EventValidator")
    @Test
    public void createEventFailWithWrongData400() throws Exception {
        // Given
        EventDto.CreateOrUpdate eventDto = EventDto.CreateOrUpdate.builder()
                .name("test")
                .description("test event")
                .beginEnrollmentDateTime(LocalDateTime.of(2018, 4, 30, 8, 0))
                .closeEnrollmentDateTime(LocalDateTime.of(2018, 3, 5, 23, 59))
                .beginEventDateTime(LocalDateTime.of(2018, 2, 10, 9, 0))
                .endEventDateTime(LocalDateTime.of(2018, 1, 10, 14, 0))
                .location("어디선가..")
                .basePrice(0)
                .maxPrice(20000)
                .build();

        // When & Then
        mockMvc.perform(post("/api/events")
                .header(HttpHeaders.AUTHORIZATION, bearer())
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(eventDto)))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void eventList200() throws Exception {
        // Given
        save30Events();

        // When & Then
        this.mockMvc.perform(get("/api/events")
                    .param("page", "1")
                    .param("size", "10")
                    .param("sort", "name"))
                .andExpect(status().isOk())
                .andDo(document("list-event"))
                .andExpect(jsonPath("_embedded.eventList[0].id").hasJsonPath())
                .andExpect(jsonPath("_embedded.eventList[0]._links.self").hasJsonPath())
                .andExpect(jsonPath("page").hasJsonPath())
                .andExpect(jsonPath("_links").hasJsonPath())
                .andExpect(jsonPath("_links.next").hasJsonPath())
                .andExpect(jsonPath("_links.prev").hasJsonPath())
                .andExpect(jsonPath("_links.profile").hasJsonPath())
                .andDo(document("list-events",
                    relaxedLinks(
                        linkWithRel("self").description("Link to self"),
                        linkWithRel("profile").description("Link to profile"),
                        linkWithRel("first").description("Link to the first page").optional(),
                        linkWithRel("next").description("Link to the next page").optional(),
                        linkWithRel("prev").description("Link to the previous page").optional()
                    ),
                    requestParameters(
                        parameterWithName("page").description("Page to query, 0 is default.").optional(),
                        parameterWithName("size").description("Size of a page, 20 is default.").optional(),
                        parameterWithName("sort").description("Property,Property(,ACS|DESC) format, unsorted is default.")
                    ),
                    relaxedResponseFields(
                        fieldWithPath("_embedded.eventList").description("Event list resource"),
                        fieldWithPath("_links").description("Links"),
                        fieldWithPath("page").description("Page")
                    )
                ));
    }

    private void save30Events() {
        IntStream.range(0, 30).forEach(i -> saveEvent());
    }

    private Event saveEvent() {
        return this.eventRepository.save(this.modelMapper.map(this.createEventDto(), Event.class));
    }

    @Test
    public void getEvent200() throws Exception {
        // Given
        var event = this.saveEvent();

        // When & Then
        this.mockMvc.perform(RestDocumentationRequestBuilders.get("/api/events/{id}", event.getId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(event.getId()))
                .andExpect(jsonPath("name").value(event.getName()))
                .andExpect(jsonPath("_links.self").hasJsonPath())
                .andExpect(jsonPath("_links.profile").hasJsonPath())
                .andDo(document("get-event",
                    pathParameters(
                        parameterWithName("id").description("identifier of an event")
                    )
                ));
    }

    @Test
    public void getEvent404() throws Exception {
        this.mockMvc.perform(get("/api/events/3923423"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void updateEvent200() throws Exception {
        // Given
        var event = this.saveEvent();
        EventDto.CreateOrUpdate updateDto = this.createEventDto();
        updateDto.setName("updated event");

        // When & Then
        this.mockMvc.perform(put("/api/events/{id}", event.getId())
                    .header(HttpHeaders.AUTHORIZATION, bearer())
                    .contentType(MediaType.APPLICATION_JSON_UTF8)
                    .content(this.objectMapper.writeValueAsString(updateDto)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value(updateDto.getName()))
                .andExpect(jsonPath("_links.self").hasJsonPath())
                .andExpect(jsonPath("_links.profile").hasJsonPath())
                .andDo(document("update-event"))
        ;
    }

}
