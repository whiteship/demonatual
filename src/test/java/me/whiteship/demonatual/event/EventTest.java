package me.whiteship.demonatual.event;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EventTest {

    @Test
    public void builder() {
        Event event = Event.builder()
                .basePrice(100)
                .maxPrice(200)
                .build();

        assertThat(event.getBasePrice()).isEqualTo(100);
        assertThat(event.getMaxPrice()).isEqualTo(200);
    }

    @Test
    public void constructor() {
        Event event = new Event();
        event.setBasePrice(100);
        event.setMaxPrice(200);

        assertThat(event.getBasePrice()).isEqualTo(100);
        assertThat(event.getMaxPrice()).isEqualTo(200);
    }

}