package me.whiteship.demonatual.user;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    @Test
    public void usernameNotFoundException() {
        // Given
        String email = "keesun@email.com";
        UserRepository userRepository = mock(UserRepository.class);
        PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();
        UserService service = new UserService(userRepository, passwordEncoder);
        when(userRepository.findByEmail(email)).thenReturn(Optional.empty());

        // When
        try {
            service.loadUserByUsername(email);
            fail("exception expected");
        } catch (UsernameNotFoundException e) {
            // Then
            assertThat(e.getMessage()).contains(email);
        }
    }

    @Test
    public void findByUsername() {
        // Given
        String email = "keesun@email.com";
        String password = "password";
        User user = User.builder()
                .email(email)
                .password(password)
                .roles(Set.of(UserRole.ADMIN, UserRole.USER))
                .build();
        UserRepository userRepository = mock(UserRepository.class);
        PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();
        UserService service = new UserService(userRepository, passwordEncoder);
        when(userRepository.findByEmail(email)).thenReturn(Optional.of(user));

        // When
        UserDetails userDetails = service.loadUserByUsername(email);

        // Then
        assertThat(userDetails.getUsername()).isEqualTo(email);
        assertThat(userDetails.getPassword()).isEqualTo(password);
        assertThat(userDetails.getAuthorities())
                .extracting(GrantedAuthority::getAuthority)
                .contains("ROLE_USER")
                .contains("ROLE_ADMIN");
    }


}