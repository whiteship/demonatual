package me.whiteship.demonatual.event;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import java.time.LocalDateTime;

@Component
public class EventValidator {

    public void validate(Event event, Errors errors) {
        LocalDateTime endEventDateTime = event.getEndEventDateTime();
        LocalDateTime beginEventDateTime = event.getBeginEventDateTime();
        LocalDateTime closeEnrollmentDateTime = event.getCloseEnrollmentDateTime();
        LocalDateTime beginEnrollmentDateTime = event.getBeginEnrollmentDateTime();

        if (endEventDateTime.isBefore(beginEventDateTime) ||
        endEventDateTime.isBefore(beginEnrollmentDateTime) ||
        endEventDateTime.isBefore(closeEnrollmentDateTime)) {
            errors.rejectValue("endEventDateTime", "Invalid.DateTime", "Invalid endEventDateTime");
        }

        if (beginEventDateTime.isBefore(closeEnrollmentDateTime) ||
        beginEventDateTime.isBefore(beginEnrollmentDateTime)) {
            errors.rejectValue("beginEventDateTime", "Invalid.DateTime", "Invalid beginEventDateTime");
        }

        if (closeEnrollmentDateTime.isBefore(beginEnrollmentDateTime)) {
            errors.rejectValue("closeEnrollmentDateTime", "Invalid.DateTime", "Invalid closeEnrollmentDateTime");
        }
    }
}
