package me.whiteship.demonatual.event;

public enum EventStatus {

    DRAFT, PUBLISHED, BEGAN_ENROLLMEND, CLOSED_ENROLLMENT, STARTED, ENDED

}
