package me.whiteship.demonatual.event;

import lombok.extern.slf4j.Slf4j;
import me.whiteship.demonatual.common.ErrorResource;
import me.whiteship.demonatual.user.CurrentUser;
import me.whiteship.demonatual.user.User;
import me.whiteship.demonatual.user.UserAdapter;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
@RequestMapping("/api/events")
@Slf4j
public class EventController {

    private ModelMapper modelMapper;

    private EventRepository eventRepository;

    private EventValidator eventValidator;

    public EventController(ModelMapper modelMapper, EventRepository eventRepository, EventValidator eventValidator) {
        this.modelMapper = modelMapper;
        this.eventRepository = eventRepository;
        this.eventValidator = eventValidator;
    }

    @PostMapping
    public ResponseEntity createEvent(@RequestBody @Valid EventDto.CreateOrUpdate eventDto,
                                      Errors errors,
                                      @CurrentUser User user) {
        if (errors.hasErrors()) {
            return badRequest(errors);
        }
        Event event = modelMapper.map(eventDto, Event.class);

        eventValidator.validate(event, errors);
        if (errors.hasErrors()) {
            return badRequest(errors);
        }

        event.update();
        event.setOwner(user);
        Event newEvent = eventRepository.save(event);
        URI locationUri = linkTo(EventController.class).slash(newEvent.getId()).toUri();

        EventResource eventResource = new EventResource(newEvent, user, linkToProfile());
        eventResource.add(linkTo(EventController.class).withRel("events"));
        return ResponseEntity.created(locationUri).body(eventResource);
    }

    @GetMapping("/{id}")
    public ResponseEntity getEvent(@PathVariable Integer id, @CurrentUser User user) {
        Optional<Event> byId = this.eventRepository.findById(id);
        if (!byId.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        EventResource eventResource = new EventResource(byId.get(), user, linkToProfile());
        return ResponseEntity.ok(eventResource);
    }

    private ResponseEntity badRequest(Errors errors) {
        return ResponseEntity.badRequest().body(new ErrorResource(errors));
    }

    @GetMapping
    public ResponseEntity getEvents(Pageable pageable,
                                    PagedResourcesAssembler<Event> assembler,
                                    @Nullable @CurrentUser User user) {
        if (user == null) {
            log.info("anonymous");
        } else {
            log.info("use is {} {}", user.getId(), user.getEmail());
        }

        Page<Event> events = this.eventRepository.findAll(pageable);
        PagedResources<Resource<Event>> resources = assembler.toResource(events, e -> new EventResource(e, user));
        resources.add(linkToProfile());
        return ResponseEntity.ok(resources);
    }

    private Link linkToProfile() {
        return new Link("/docs/index.html").withRel("profile");
    }

    @PutMapping("/{id}")
    public ResponseEntity updateEvent(@PathVariable Integer id,
                                      @RequestBody @Valid EventDto.CreateOrUpdate update,
                                      Errors errors,
                                      @CurrentUser User user) {
        if (errors.hasErrors()) {
            return badRequest(errors);
        }

        Optional<Event> byId = this.eventRepository.findById(id);
        if (!byId.isPresent()) {
            return ResponseEntity.notFound().build();
        }
        Event exitingEvent = byId.get();

        if (!exitingEvent.getOwner().equals(user)) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }

        this.modelMapper.map(update, exitingEvent);
        this.eventValidator.validate(exitingEvent, errors);

        if (errors.hasErrors()) {
            return badRequest(errors);
        }

        EventResource eventResource = new EventResource(exitingEvent, user, linkToProfile());
        return ResponseEntity.ok(eventResource);
    }
}
