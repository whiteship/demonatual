package me.whiteship.demonatual.event;

import me.whiteship.demonatual.user.User;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.Resource;
import org.springframework.lang.Nullable;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

public class EventResource extends Resource<Event> {

    public EventResource(Event event, @Nullable User user, Link... links) {
        super(event, links);
        add(linkTo(EventController.class).slash(event.getId()).withSelfRel());
        if (event.getOwner().equals(user)) {
            add(linkTo(EventController.class).slash(event.getId()).withRel("update"));
        }
    }
}
