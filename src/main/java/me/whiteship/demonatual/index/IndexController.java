package me.whiteship.demonatual.index;

import me.whiteship.demonatual.event.EventController;
import org.springframework.hateoas.ResourceSupport;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;

@RestController
public class IndexController {

    @GetMapping("/api")
    public ResourceSupport index() {
        var resource = new ResourceSupport();
        resource.add(linkTo(EventController.class).withRel("events"));
        return resource;
    }

}
