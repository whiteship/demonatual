package me.whiteship.demonatual.user;

import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class UserAdapter extends User {

    private me.whiteship.demonatual.user.User user;

    public UserAdapter(me.whiteship.demonatual.user.User user) {
        super(user.getEmail(), user.getPassword(), authorities(user.getRoles()));
        this.user = user;
    }

    private static Set<SimpleGrantedAuthority> authorities(Set<UserRole> roles) {
        return roles.stream()
                .map(r -> new SimpleGrantedAuthority("ROLE_" + r.name()))
                .collect(Collectors.toSet());
    }
}
