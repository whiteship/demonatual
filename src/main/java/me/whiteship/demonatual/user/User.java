package me.whiteship.demonatual.user;

import lombok.*;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "S_User")
@Getter @Setter @EqualsAndHashCode(of = "id")
@Builder @NoArgsConstructor @AllArgsConstructor
public class User {

    @Id @GeneratedValue
    private Integer id;

    private String email;

    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    @Enumerated(EnumType.STRING)
    private Set<UserRole> roles;

}
