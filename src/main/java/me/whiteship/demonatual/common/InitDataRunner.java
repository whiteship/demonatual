package me.whiteship.demonatual.common;

import me.whiteship.demonatual.user.User;
import me.whiteship.demonatual.user.UserRole;
import me.whiteship.demonatual.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
public class InitDataRunner implements ApplicationRunner {

    @Autowired
    UserService userService;

    @Autowired
    AppProperties appProperties;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        User admin = User.builder()
                .email(appProperties.getAdminUsername())
                .password(appProperties.getAdminPassword())
                .roles(Set.of(UserRole.ADMIN, UserRole.USER))
                .build();
        userService.createUser(admin);

        User user = User.builder()
                .email(appProperties.getUserUsername())
                .password(appProperties.getUserPassword())
                .roles(Set.of(UserRole.USER))
                .build();
        userService.createUser(user);
    }
}
