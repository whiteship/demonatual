package me.whiteship.demonatual.common;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Documented
@Retention(RetentionPolicy.SOURCE)
public @interface Description {
    String value() default "";
}
